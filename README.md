Test project for ReTech lab Python job vacancy.
=

### Functionality:
- Authentication
- Choose active company
- Binding users to companies
- ToDo Lists and items CRUD operations

---

# Preparing the project
## Installation
```
    pip install -r requirements
    python manage.py migrate
    
```
    
## Setting up initial data in the admin
```
    python manage.py createsuperuser
    
```
    
After creating a superuser you can login in the admin panel /admin and create some companies and users for testing purposes.
    
## Running all the tests

The code is widely covered by the tests. You can run them to make sure everything is fine at least in the beginning :)
```
    python manage.py test
```

---
# API description
## Authentication
#### Registration

```
    POST /api/register
```

Parameters:

|Name|Description|Mandatory|
|----|-----------|--------|
|email|New user's email|Yes|
|password|New user's password|Yes|
|first_name|New user's first name|No|
|last_name|New user's last name|No|

#### Login

Loggs in a user with given credentials and returns user info and *bound companies info* (so it will be more convinient to choose a company right after login and continue using API)
```
    POST /api/login
```
    
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|email|Existing user's email|Yes|
|password|Existing user's password|Yes|

#### Logout
```
    GET /api/logout
```
    
## Choose active company

After a user successfully logged-in in order to continue using API active company should be set (don't forget it returns in the log-in response).
```
    POST /api/set_current_company
```
    
    
Parameters:

|Name|Description|Mandatory|
|----|-----------|---------|
|company_id|An ID of one of the user's bound companies|Yes|


## Bind company to user

In order to choose active company a user should have at least one bound company. The only admin users can bind companies to users (admin users have 'is_staff' flag set to True)
```
    POST /api/bind_user_to_company
```
    
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|user_id|An ID of a user|Yes|
|company_id|An ID of a company|Yes|

## ToDo lists
There are typical DRF CRUD operations for ToDo lists.

#### Get all available lists
```
    GET /api/todo_lists
```
#### Create a new list
```
    POST /api/todo_lists
```
    
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|title|ToDo list title|Yes|
|description|Todo list description|No|

#### Get a list
```
    GET /api/todo_lists/<list_id>
```
#### Update a list
```
    PUT /api/todo_lists/<list_id>
```
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|title|ToDo list title|No|
|description|Todo list description|No|

#### Delete a list
```
    DELETE /api/todo_lists/<list_id>
```
## ToDo list items
There are typical DRF CRUD operations for ToDo list items.

#### Get all available items for a list
```
    GET /api/todo_lists/<list_id>
```
#### Create a new list item
```
    POST /api/todo_lists/<list_id>
```
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|title|ToDo list title|Yes|
|description|Todo list description|No|
|is_done|Flag representing the completion of the item. Default - False|No|

#### Get a list item
```
    GET /api/todo_lists/<list_id>/<item_id>
```    
#### Update a list item
```
    PUT /api/todo_lists/<list_id>/<item_id>
```
Parameters:

|Name|Description|Mandatory|
|-|-|-|
|title|ToDo list title|No|
|description|Todo list description|No|
|is_done|Flag representing the completion of the item|No|

#### Delete a list item
```
    DELETE /api/todo_lists/<list_id>/<item_id>
```