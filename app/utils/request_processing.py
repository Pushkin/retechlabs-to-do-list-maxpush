from rest_framework.response import Response
from rest_framework import status


def catch_request_errors_decorator(func):
    def handle(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            if hasattr(e, 'status_code'):
                status_code = e.status_code
            return Response(str(e), status=status_code)

    return handle


def try_get_request_argument_value(argument_name, request_data):
    value = request_data.get(argument_name, None)
    if value is None:
        raise Exception('Request error: a wrong set of arguments was passed')

    return value
