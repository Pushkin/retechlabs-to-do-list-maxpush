from django.conf.urls import url, include
from app.views import users as users_views, to_do_lists as to_do_lists_views


todo_list_list = to_do_lists_views.ToDoListViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
todo_list_detail = to_do_lists_views.ToDoListViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

todo_item_list = to_do_lists_views.ToDoListItemViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
todo_item_detail = to_do_lists_views.ToDoListItemViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    url(r'register', users_views.RegisterView.as_view(), name='register'),
    url(r'login', users_views.LoginView.as_view(), name='login'),
    url(r'logout', users_views.LogoutView.as_view(), name='logout'),
    url(r'set_current_company', users_views.SetCurrentCompanyView.as_view(), name='set-current-company'),
    url(r'bind_user_to_company', users_views.BindUserToCompanyView.as_view(), name='bind-user-to-company'),
    url(r'^todo_lists/$', todo_list_list, name='todo-list-list'),
    url(r'^todo_lists/(?P<pk>[0-9]+)/$', todo_list_detail, name='todo-list-detail'),
    url(r'^todo_items/(?P<list_id>[0-9]+)/$', todo_item_list, name='todo-item-list'),
    url(r'^todo_items/(?P<list_id>[0-9]+)/(?P<pk>[0-9]+)$', todo_item_detail, name='todo-item-detail'),
]
