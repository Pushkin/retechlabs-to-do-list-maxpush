import json
from django.contrib.auth import get_user_model
from app.models import Company
from .base import BaseToDoTestCase


class UserRegisterTestCase(BaseToDoTestCase):
    def test_register_user(self):
        response = self._send_request(
            'register',
            method=self.METHOD_POST,
            request_data={
                'email': self.TEST_EMAIL_ADDRESS,
                'first_name': self.TEST_FIRST_NAME,
                'last_name': self.TEST_LAST_NAME,
                'password': self.TEST_PASSWORD
            }
        )

        user_model = get_user_model()
        registered_user = user_model.objects.get(email=self.TEST_EMAIL_ADDRESS)

        self.assertEqual(response.status_code, 201)
        self.assertNotEqual(registered_user, None)


class UserAuthTestCase(BaseToDoTestCase):
    def setUp(self):
        super(UserAuthTestCase, self).setUp()
        self._create_test_user()

    def test_login_user(self):
        response = self._send_login_request()
        response_data = json.loads(response.data)
        user_data = response_data.get('user')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(user_data.get('first_name'), self.TEST_FIRST_NAME)
        self.assertEqual(user_data.get('last_name'), self.TEST_LAST_NAME)

    def test_logout_user(self):
        login_response = self._send_login_request()
        logout_response = self._send_logout_request()

        self.assertEqual(login_response.status_code, 200)
        self.assertEqual(logout_response.status_code, 200)


class SetUserCurrentCompanyTestCase(BaseToDoTestCase):
    def setUp(self):
        super(SetUserCurrentCompanyTestCase, self).setUp()
        test_user = self._create_test_user()
        self._create_test_companies()
        company = Company.objects.first()
        self.bound_company = company
        self.unbound_company = Company.objects.last()
        test_user.bound_companies.add(company)
        test_user.save()


    def _send_set_current_company_request(self, company_id):
        set_current_company_request_data = {
            'company_id': company_id
        }

        response = self._send_request(
            'set-current-company',
            method=self.METHOD_POST,
            request_data=set_current_company_request_data
        )

        return response

    def test_set_current_company(self):
        unauthorized_response = self._send_set_current_company_request(self.bound_company.id)
        login_response = self._send_login_request()
        authorized_unbound_company_response = self._send_set_current_company_request(self.unbound_company.id)
        authorized_bound_company_response = self._send_set_current_company_request(self.bound_company.id)
        second_authorized_bound_company_response = self._send_set_current_company_request(self.bound_company.id)

        self.assertEqual(unauthorized_response.status_code, 403)
        self.assertEqual(login_response.status_code, 200)
        self.assertEqual(authorized_unbound_company_response.status_code, 403)
        self.assertEqual(authorized_bound_company_response.status_code, 200)
        self.assertEqual(second_authorized_bound_company_response.status_code, 403)
        self.assertEqual(
            get_user_model().objects.get(email=self.TEST_EMAIL_ADDRESS).active_company_id,
            self.bound_company.id
        )


class BindUserToCompanyTestCase(BaseToDoTestCase):
    def setUp(self):
        self._create_test_companies()
        self._create_test_user()

    def _send_bind_user_to_company_request(self, user_id, company_id):
        bind_user_to_company_request_data = {
            'user_id': user_id,
            'company_id': company_id,
        }

        response = self._send_request(
            'bind-user-to-company',
            method=self.METHOD_POST,
            request_data=bind_user_to_company_request_data
        )

        return response

    def test_bind_user_to_company(self):
        login_response = self._send_login_request()
        user_data = json.loads(login_response.data)['user']
        company_id = Company.objects.first().id

        regular_user_response = self._send_bind_user_to_company_request(
            user_id=user_data['id'],
            company_id=company_id
        )

        user = get_user_model().objects.get(id=user_data['id'])
        user.is_staff = True
        user.save()

        self._send_logout_request()
        login_response = self._send_login_request()

        superuser_response = self._send_bind_user_to_company_request(
            user_id=user.id,
            company_id=company_id
        )

        user = get_user_model().objects.get(id=user_data['id'])

        self.assertEqual(login_response.status_code, 200)
        self.assertEqual(regular_user_response.status_code, 403)
        self.assertEqual(superuser_response.status_code, 200)
        self.assertTrue(any(cmp for cmp in list(user.bound_companies.all()) if cmp.id == company_id))