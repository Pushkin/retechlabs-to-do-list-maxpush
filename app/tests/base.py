import json
from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from app.models import Company


class BaseToDoTestCase(TestCase):
    TEST_EMAIL_ADDRESS = 'test123@gmail.com'
    TEST_PASSWORD = '123'
    TEST_FIRST_NAME = 'John'
    TEST_LAST_NAME = 'Johnson'

    TEST_COMPANY_NAMES = ['Test Company 1', 'Test Company 2', '-MaxPush Inc.-']

    TEST_CONTENT_TYPE = 'application/json'

    METHOD_GET = 'get'
    METHOD_POST = 'post'
    METHOD_PUT = 'put'
    METHOD_PATCH = 'patch'
    METHOD_DELETE = 'delete'

    def setUp(self):
        self.client = Client()

    def _send_request(self, reverse_url, method=METHOD_GET, request_data=None, url_data=None):
        url = reverse(reverse_url, kwargs=url_data)

        if request_data is not None:
            request_data = json.dumps(request_data)

        client_method = self.client.get

        if method == self.METHOD_POST:
            client_method = self.client.post
        elif method == self.METHOD_PUT:
            client_method = self.client.put
        elif method == self.METHOD_PATCH:
            client_method = self.client.patch
        elif method == self.METHOD_DELETE:
            client_method = self.client.delete

        return client_method(url, data=request_data, content_type=self.TEST_CONTENT_TYPE)

    def _send_login_request(self):
        return self._send_request(
            'login',
            method=self.METHOD_POST,
            request_data={
                'email': self.TEST_EMAIL_ADDRESS,
                'password': self.TEST_PASSWORD,
            }
        )

    def _send_logout_request(self):
        return self._send_request('logout')

    def _create_test_user(self):
        user_model = get_user_model()
        return user_model.objects.create_user(
            username=self.TEST_EMAIL_ADDRESS,
            email=self.TEST_EMAIL_ADDRESS,
            password=self.TEST_PASSWORD,
            first_name=self.TEST_FIRST_NAME,
            last_name=self.TEST_LAST_NAME,
        )

    def _create_test_companies(self):
        return [Company.objects.create(name=name) for name in self.TEST_COMPANY_NAMES]


class BasePreparedTestCase(BaseToDoTestCase):
    def setUp(self):
        super(BasePreparedTestCase, self).setUp()

        self.user = self._create_test_user()
        self.companies = self._create_test_companies()
        for company in self.companies:
            self.user.bound_companies.add(company)
        self.user.active_company = self.companies[0]
        self.user.save()
        self._send_login_request()

    def _send_unauthorized_request(self, reverse_url, method, request_data=None, url_data=None):
        self._send_logout_request()
        response = self._send_request(reverse_url, method, request_data, url_data)
        self._send_login_request()
        return response
