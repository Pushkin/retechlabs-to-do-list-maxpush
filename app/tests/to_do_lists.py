import json
from app.models import ToDoList, ToDoListItem
from app.serializers import ToDoListSerializer, ToDoListItemSerializer
from .base import BasePreparedTestCase


class ToDoListTestCase(BasePreparedTestCase):
    TEST_LIST_DATA = {
        'title': 'Test list',
        'description': 'This is a test list'
    }

    def _send_create_list_request(self):
        return self._send_request(
            'todo-list-list',
            self.METHOD_POST,
            request_data=self.TEST_LIST_DATA
        )

    def test_create_list(self):
        response = self._send_create_list_request()
        unauthorized_response = self._send_unauthorized_request(
            'todo-list-list',
            self.METHOD_POST,
            request_data=self.TEST_LIST_DATA
        )

        self.assertEqual(response.status_code, 201)
        self.assertEqual(unauthorized_response.status_code, 403)
        self.assertEqual(ToDoList.objects.all().count(), 1)
        self.assertEqual(ToDoList.objects.first().title, self.TEST_LIST_DATA['title'])

    def test_get_lists(self):
        # Create a list which theoretically was created from name of the other company
        foreign_list = ToDoList.objects.create(
            title='Some foreign list',
            description='This is a list which is not related to the current user\'s company',
            company_id=self.companies[2].id
        )

        # Then create a list from current user
        create_response = self._send_create_list_request()

        # Then try to get all the available lists
        list_response = self._send_request('todo-list-list')

        # Then incorrect list
        foreign_list_response = self._send_request(
            'todo-list-detail',
            url_data={'pk': foreign_list.id},
        )

        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(list_response.status_code, 200)

        # Expected foreign list will not be available for the current user so it will be not found
        self.assertEqual(foreign_list_response.status_code, 404)

        self.assertEqual(ToDoList.objects.all().count(), 2)

        # As we have 2 lists, we expect to see only the one which was created from the current user's active company
        self.assertEqual(len(list_response.data), 1)
        self.assertEqual(list_response.data[0]['title'], self.TEST_LIST_DATA['title'])
        self.assertEqual(list_response.data[0]['company_id'], self.user.active_company_id)

    def test_update_list(self):
        updated_title = 'New Title'
        create_response = self._send_create_list_request()
        list_data = ToDoListSerializer(ToDoList.objects.first()).data
        list_data['title'] = updated_title

        update_response = self._send_request(
            'todo-list-detail',
            method=self.METHOD_PUT,
            request_data=list_data,
            url_data={'pk': list_data['id']}
        )

        unauth_update_response = self._send_unauthorized_request(
            'todo-list-detail',
            method=self.METHOD_PUT,
            request_data=list_data,
            url_data={'pk': list_data['id']}
        )

        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(update_response.status_code, 200)
        self.assertEqual(unauth_update_response.status_code, 403)
        self.assertEqual(ToDoListSerializer(ToDoList.objects.first()).data['title'], updated_title)

    def test_delete_list(self):
        create_response = self._send_create_list_request()
        list_data = ToDoListSerializer(ToDoList.objects.first()).data
        count_after_create = ToDoList.objects.all().count()

        delete_response = self._send_request(
            'todo-list-detail',
            method=self.METHOD_DELETE,
            url_data={'pk': list_data['id']}
        )

        unauth_delete_response = self._send_unauthorized_request(
            'todo-list-detail',
            method=self.METHOD_DELETE,
            url_data={'pk': list_data['id']}
        )

        count_after_delete = ToDoList.objects.all().count()

        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(delete_response.status_code, 204)
        self.assertEqual(unauth_delete_response.status_code, 403)
        self.assertEqual(count_after_create, 1)
        self.assertEqual(count_after_delete, 0)


class ToDoListItemTestCase(BasePreparedTestCase):
    def setUp(self):
        super(ToDoListItemTestCase, self).setUp()

        self.own_list = ToDoList.objects.create(
            title='Test list',
            description='This is a list which is related to the current user\'s company',
            company_id=self.companies[0].id
        )

        self.foreign_list = ToDoList.objects.create(
            title='Some foreign list',
            description='This is a list which is not related to the current user\'s company',
            company_id=self.companies[2].id
        )

        self.DEFAULT_LIST_ID = self.own_list.id
        self.DEFAULT_CREATE_ITEM_DATA = {
            'title': 'To buy milk',
            'description': 'Go to local shop and buy a bottle of milk'
        }

    # fields = ('title', 'description', 'is_done', 'list_id')

    def _create_test_todo_items(self):
        self.own_items = []
        self.foreign_items = []
        for i in range(3):
            self.own_items.append(ToDoListItem.objects.create(
                title='Item {}'.format(i),
                description='Description'.format(i),
                list_id=self.own_list.id
            ))
        for i in range(4, 7):
            self.foreign_items.append(ToDoListItem.objects.create(
                title='Item {}'.format(i),
                description='Description'.format(i),
                list_id=self.foreign_list.id
            ))

    def _send_create_item_request(self, data=None, list_id=None):
        data = data or self.DEFAULT_CREATE_ITEM_DATA
        list_id = list_id or self.DEFAULT_LIST_ID
        data['list_id'] = list_id
        return self._send_request(
            'todo-item-list',
            method=self.METHOD_POST,
            request_data=data,
            url_data={'list_id': list_id}
        )

    def test_create_todo_list_item(self):
        create_response = self._send_create_item_request()
        foreign_list_response = self._send_create_item_request(list_id=self.foreign_list.id)

        self.assertEqual(create_response.status_code, 201)
        self.assertEqual(foreign_list_response.status_code, 403)
        self.assertEqual(ToDoListItem.objects.first().title, self.DEFAULT_CREATE_ITEM_DATA['title'])

    def test_get_todo_list_items(self):
        self._create_test_todo_items()

        get_own_items_response = self._send_request(
            'todo-item-list',
            url_data={'list_id': self.own_list.id}
        )

        get_foreign_items_response = self._send_request(
            'todo-item-list',
            url_data={'list_id': self.foreign_list.id}
        )

        self.assertEqual(get_own_items_response.status_code, 200)
        self.assertEqual(get_foreign_items_response.status_code, 403)

        self.assertEqual(len(get_own_items_response.data), 3)
        self.assertEqual(ToDoListItem.objects.all().count(), 6)

    def test_update_list_item(self):
        self._create_test_todo_items()
        updated_item_title = 'Updated Title'

        data_to_update = ToDoListItemSerializer(self.own_items[0]).data
        data_to_update['title'] = updated_item_title

        foreign_data_to_update = ToDoListItemSerializer(self.foreign_items[0]).data
        foreign_data_to_update['title'] = updated_item_title

        update_response = self._send_request(
            'todo-item-detail',
            method=self.METHOD_PUT,
            request_data=data_to_update,
            url_data={'list_id': data_to_update['list_id'], 'pk': data_to_update['id']}
        )

        foreign_update_response = self._send_request(
            'todo-item-detail',
            method=self.METHOD_PUT,
            request_data=foreign_data_to_update,
            url_data={'list_id': foreign_data_to_update['list_id'], 'pk': foreign_data_to_update['id']}
        )

        self.assertEqual(update_response.status_code, 200)
        self.assertEqual(foreign_update_response.status_code, 403)
        self.assertEqual(ToDoListItem.objects.get(id=data_to_update['id']).title, updated_item_title)

    def test_delete_list_item(self):
        self._create_test_todo_items()

        delete_own_response = self._send_request(
            'todo-item-detail',
            method=self.METHOD_DELETE,
            url_data={'list_id': self.own_list.id, 'pk': self.own_items[0].id}
        )

        delete_foreign_response = self._send_request(
            'todo-item-detail',
            method=self.METHOD_DELETE,
            url_data={'list_id': self.foreign_list.id, 'pk': self.foreign_items[0].id}
        )

        self.assertEqual(delete_own_response.status_code, 204)
        self.assertEqual(delete_foreign_response.status_code, 403)
        self.assertEqual(ToDoListItem.objects.all().count(), 5)


