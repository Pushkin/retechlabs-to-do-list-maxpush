from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy


class Company(models.Model):
    class Meta:
        verbose_name_plural = ugettext_lazy("Companies")

    name = models.CharField(max_length=100, null=False)

    def __str__(self):
        return self.name


class ToDoList(models.Model):
    title = models.CharField(max_length=100, null=False)
    description = models.TextField(null=True)
    company = models.ForeignKey(Company, null=False)

    @classmethod
    def get_all_by_company_id(cls, company_id):
        return cls.objects.filter(company_id=company_id)

    @classmethod
    def get_company_id_by_list_id(cls, list_id):
        return cls.objects.get(id=list_id).company_id

    @classmethod
    def does_exist(cls, list_id):
        return cls.objects.filter(id=list_id).count() != 0

    def __str__(self):
        return self.title


class ToDoListItem(models.Model):
    title = models.CharField(max_length=150, null=False)
    description = models.TextField(null=True)
    is_done = models.BooleanField(null=False, default=False)
    list = models.ForeignKey(ToDoList, null=False)

    @classmethod
    def get_all_by_list_id(cls, list_id):
        return cls.objects.filter(list_id=list_id)

    def __str__(self):
        return "{}: {}".format(self.list.title, self.title)


class User(AbstractUser):
    bound_companies = models.ManyToManyField(Company, null=True, related_name='users_%(class)s_binding')
    active_company = models.ForeignKey(Company, null=True, related_name='user_active_%(class)s')

    def get_bound_companies(self):
        return self.bound_companies.all()

    def set_active_company(self, company_id):
        self.active_company = Company.objects.get(id=company_id)
        self.save()

    def flush_active_company(self):
        self.active_company = None
        self.save()
