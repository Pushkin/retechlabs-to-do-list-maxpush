from django.contrib.auth import get_user_model
from rest_framework import serializers
from app.models import Company, ToDoList, ToDoListItem


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name')


class ToDoListSerializer(serializers.ModelSerializer):
    company_id = serializers.IntegerField()

    class Meta:
        model = ToDoList
        fields = ('id', 'title', 'description', 'company_id')


class ToDoListItemSerializer(serializers.ModelSerializer):
    list_id = serializers.IntegerField()

    class Meta:
        model = ToDoListItem
        fields = ('id', 'title', 'description', 'is_done', 'list_id')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'first_name', 'last_name', 'active_company_id')
