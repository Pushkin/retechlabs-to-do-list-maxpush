import json
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response
from app.serializers import ToDoListSerializer, ToDoListItemSerializer
from app.models import ToDoList, ToDoListItem


class ToDoListViewSet(viewsets.ModelViewSet):
    serializer_class = ToDoListSerializer
    permission_classes = (IsAuthenticated, )

    def create(self, request, *args, **kwargs):
        request_data = json.loads(request.body)
        request_data['company_id'] = request.user.active_company_id

        serialized = self.get_serializer(data=request_data)
        serialized.is_valid(raise_exception=True)

        self.perform_create(serialized)

        return Response(status=status.HTTP_201_CREATED)

    def get_queryset(self):
        company_id = self.request.user.active_company_id
        result = ToDoList.get_all_by_company_id(company_id)
        return result or []

    def check_object_permissions(self, request, obj):
        company_id = self.request.user.active_company_id
        if obj.company_id != company_id:
            self.permission_denied(
                request, message="The list does not belong to your company"
            )

        super(ToDoListViewSet, self).check_object_permissions(request, obj)


class ToDoListItemViewSet(viewsets.ModelViewSet):
    serializer_class = ToDoListItemSerializer
    permission_classes = (IsAuthenticated, )

    def create(self, request, *args, **kwargs):
        request_data = json.loads(request.body)
        request_data['company_id'] = request.user.active_company_id

        list_id = request_data['list_id']

        if not ToDoList.does_exist(list_id):
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data={'error_message': 'There is no such todo list'}
            )

        self._check_list_access(request, list_id)

        serialized = self.get_serializer(data=request_data)
        serialized.is_valid(raise_exception=True)

        self.perform_create(serialized)

        return Response(status=status.HTTP_201_CREATED)

    def get_queryset(self):
        list_id = self.kwargs.get('list_id', None)
        if not list_id:
            return []

        self._check_list_access(self.request, list_id)

        return ToDoListItem.get_all_by_list_id(list_id)

    def check_object_permissions(self, request, obj):
        company_id = self.request.user.active_company_id
        if obj.list.company_id != company_id:
            self.permission_denied(
                request, message="The list does not belong to your company"
            )

        super(ToDoListItemViewSet, self).check_object_permissions(request, obj)

    def _check_list_access(self, request, list_id):
        company_id = request.user.active_company_id
        if ToDoList.get_company_id_by_list_id(list_id) != company_id:
            self.permission_denied(self.request, 'You don\'t have access to the items of this todo list')