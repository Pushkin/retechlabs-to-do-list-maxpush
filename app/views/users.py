import json
from django.contrib.auth import get_user_model, authenticate, login, logout
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from app.serializers import UserSerializer, CompanySerializer
from app.utils.request_processing import catch_request_errors_decorator, try_get_request_argument_value
from app.models import Company


class RegisterView(APIView):

    @catch_request_errors_decorator
    def post(self, request):
        user_data = json.loads(request.body)
        serialized_data = UserSerializer(data=user_data)

        if not serialized_data.is_valid():
            raise Exception("Invalid request data")

        user_model = get_user_model()
        user_model.objects.create_user(
            username=try_get_request_argument_value('email', user_data),
            email=try_get_request_argument_value('email', user_data),
            password=try_get_request_argument_value('password', user_data),
            first_name=try_get_request_argument_value('first_name', user_data),
            last_name=try_get_request_argument_value('last_name', user_data),
        )

        return Response(status=status.HTTP_201_CREATED)


class LogoutView(APIView):
    permission_classes = (IsAuthenticated, )

    @catch_request_errors_decorator
    def get(self, request):
        # do pre-logout user actions
        request.user.flush_active_company()

        # logout user
        logout(request)

        return Response(status=status.HTTP_200_OK)


class LoginView(APIView):

    @catch_request_errors_decorator
    def post(self, request):
        request_data = json.loads(request.body)
        email = try_get_request_argument_value('email', request_data)
        password = try_get_request_argument_value('password', request_data)

        user = authenticate(
            username=email,
            password=password
        )

        if not user:
            raise Exception("Authentication failed: invalid request data")

        login(request, user)

        user_data = {
            'user': UserSerializer(user).data,
            'bound_companies': [CompanySerializer(c).data for c in user.get_bound_companies()]
        }

        return Response(status=status.HTTP_200_OK, data=json.dumps(user_data))


class BindUserToCompanyView(APIView):
    permission_classes = (IsAdminUser, )

    @catch_request_errors_decorator
    def post(self, request):
        request_data = json.loads(request.body)
        user_id = try_get_request_argument_value('user_id', request_data)
        company_id = try_get_request_argument_value('company_id', request_data)
        user_model = get_user_model()
        user = user_model.objects.get(id=user_id)
        user.bound_companies.add(Company.objects.get(id=company_id))
        user.save()

        return Response(status=status.HTTP_200_OK)


class SetCurrentCompanyView(APIView):
    permission_classes = (IsAuthenticated,)

    @catch_request_errors_decorator
    def post(self, request):
        request_data = json.loads(request.body)
        company_id = try_get_request_argument_value('company_id', request_data)

        if request.user.active_company is not None:
            self.permission_denied(request, 'You need to re-login in order to switch your active company')

        # checking if company_id belongs to a one of the user's bound companies
        if not any((cmp for cmp in list(request.user.bound_companies.all()) if cmp.id == company_id)):
            self.permission_denied(request, 'Unable to set active company: company is not bound to the user')

        request.user.set_active_company(company_id)

        return Response(status=status.HTTP_200_OK)
