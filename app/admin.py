from django.contrib import admin
from app.models import Company, ToDoListItem, User, ToDoList

admin.site.register(Company)
admin.site.register(ToDoList)
admin.site.register(ToDoListItem)
admin.site.register(User)
